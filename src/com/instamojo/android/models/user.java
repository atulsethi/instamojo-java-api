package com.instamojo.android.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="user" , catalog = "infomobi_database")
public class user {

	private Integer user_id;
	
	private String mobile_no;
	private String  name;
	private String  date_time;
	private String  email_id;
	private String  password;
	private String  UniqueId;
	private String  rdusername;
	private Integer  BiilingStatus;
	private Date created_at;
	private Date billingExpire;
	private Integer  appforceUpdate;

	
	
	
	

	
	
	@Transient
	private long  dayLeftForExpire;

	
	
	public long getDayLeftForExpire() {
		return dayLeftForExpire;
	}

	public void setDayLeftForExpire(long dayLeftForExpire) {
		this.dayLeftForExpire = dayLeftForExpire;
	}

	@Column(name="billing_expire")
	public Date getBillingExpire() {
		return billingExpire;
	}

	public void setBillingExpire(Date billingExpire) {
		this.billingExpire = billingExpire;
	}
	

	@Column(name="created_at", updatable = false , columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Column(name="BiilingStatus")
	public Integer getBiilingStatus() {
		return BiilingStatus;
	}
	
	public void setBiilingStatus(Integer biilingStatus) {
		BiilingStatus = biilingStatus;
	}
	
	@Column(name="unique_id")
	public String getUniqueId() {
		return UniqueId;
	}
	public void setUniqueId(String uniqueId) {
		UniqueId = uniqueId;
	}
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	
	@Column(name="appforceUpdate")
	public Integer getAppforceUpdate() {
		return appforceUpdate;
	}

	public void setAppforceUpdate(Integer appforceUpdate) {
		this.appforceUpdate = appforceUpdate;
	}
	
	
	@Column(name="rdusername")
	public String getRdusername() {
		return rdusername;
	}
	public void setRdusername(String rdusername) {
		this.rdusername = rdusername;
	}
	
	
	@Column(name="mobile_no")
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="date_time")
	
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	
	
	@Column(name="email_id")
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	

	  
	
}
