package com.instamojo.android.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="OrderIDRequest" , catalog = "infomobi_database")
public class GetOrderIDRequest {

	private Integer id;

  
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	  public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}
	
	
	
	
	@SerializedName("env")
    private String env;
    
  
	@SerializedName("order_id")
    private String orderID;

	
	
    private String payment_status;

    @SerializedName("buyer_name")
    private String buyerName;

    @SerializedName("buyer_email")
    private String buyerEmail;

    @SerializedName("buyer_phone")
    private String buyerPhone;

    @SerializedName("amount")
    private String amount;

    @SerializedName("description")
    private String description;

	
	@Column(name="env")
    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    


	@Column(name="payment_status")

    public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}

	@Column(name="orderID")
	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	
	@Column(name="buyer_name")
	public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }


	@Column(name="buyer_email")
    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

   
    @Column(name="buyer_phone")
    public String getBuyerPhone() {
        return buyerPhone;
    }

    public void setBuyerPhone(String buyerPhone) {
        this.buyerPhone = buyerPhone;
    }

	@Column(name="amount")

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

	@Column(name="description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
