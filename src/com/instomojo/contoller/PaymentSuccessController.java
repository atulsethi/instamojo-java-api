package com.instomojo.contoller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.rdmanager.database.DbOperation;
import com.instamojo.android.models.GetOrderIDRequest;
import com.instamojo.android.models.user;

@Controller
@RequestMapping("/PaymentSuccess")
public class PaymentSuccessController {

	@RequestMapping(method = RequestMethod.GET)
	public String printHello(@RequestParam("payment_status") String payment_status,@RequestParam("id") String id,@RequestParam("transaction_id") String transaction_id,ModelMap model) {

		//model.addAttribute("message", "Hello Spring MVC Framework!");
		
		
		GetOrderIDRequest mGetOrderIDRequest=	DbOperation.getObject(GetOrderIDRequest.class, "orderID", id);
		mGetOrderIDRequest.setPayment_status(payment_status);
		DbOperation.saveOrUpdate(mGetOrderIDRequest);
		
		
		user mUser=DbOperation.getObject(user.class, "rdusername", mGetOrderIDRequest.getBuyerName());
		mUser.setBiilingStatus(1);
		
		DbOperation.saveOrUpdate(mUser);

		
		return "PaymentSuccess";
	}
	
}
