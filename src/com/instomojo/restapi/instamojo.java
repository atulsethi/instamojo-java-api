package com.instomojo.restapi;

import java.io.IOException;
import java.util.Random;

import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.rdmanager.database.DbOperation;
import com.google.gson.Gson;
import com.instamojo.android.models.GetOrderIDRequest;
import com.instamojo.android.models.GetOrderIDResponse;
import com.instamojo.wrapper.api.ApiContext;
import com.instamojo.wrapper.api.Instamojo;
import com.instamojo.wrapper.api.InstamojoImpl;
import com.instamojo.wrapper.exception.ConnectionException;
import com.instamojo.wrapper.exception.HTTPException;
import com.instamojo.wrapper.model.PaymentOrder;
import com.instamojo.wrapper.model.PaymentOrderResponse;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

@RestController
@RequestMapping("/")
public class instamojo {
	
	
	String clientId="jAODg958lNXojVblx2yredGLpxoqsmpJcvtyZQzt";
	String clientSecert="Je2unhVAG7qpTZKkWe4Yc6lHd8svSQNu7WnRTgSQGIChDHDjVlEGT2Z62kXGlnMNOp06qkqjJQlgLW5vKF9g3uPUDkhe9t9iovTNuqC4u05iUFjDiQAgxICYPbfFR0qs";
//
//	
//	String clientId="X8SenzPOyx8bv53svRmoV0nFxK7PLgszdC9vNtQR";
//	String clientSecert="bxYBvQkoTD695olLbdACMRy92JsZkf5GFC42LyE7smsVTkYdEE4W7zdB306woT3SFKXSL0jXFp8tQNTGuaMlWrpQCokQ2I08kYoR8nLw49u2ap45qntNgb0W3UmMiKJE";

//	String clientId="clientId";
//	String clientSecert="clientSecert";

	
	
//	String RedirectUrl="http://192.168.42.219:8080/instomojo/PaymentSuccess";
//	String WebhookUrl="http://192.168.42.219:8080/instomojo/PaymentFailed";

	
	String RedirectUrl="http://infomobile.in/PaymentSuccess";
	String WebhookUrl="http://infomobile.in/PaymentFailed";
	
	@RequestMapping(value = "/getToken", method = RequestMethod.GET)
	public String token() {

		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
		RequestBody body = RequestBody.create(mediaType,
				"grant_type=client_credentials&client_id="+clientId+"&client_secret="+clientSecert);
		Request request = new Request.Builder().url("https://instamojo.com/oauth2/token/").post(body)
				.addHeader("content-type", "application/x-www-form-urlencoded").addHeader("cache-control", "no-cache")
				.build();

		try {
			Response response = client.newCall(request).execute();
			return response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	
	
	@RequestMapping(value = "/getOrderDetail/{orderId}", method = RequestMethod.GET)
	public String OrderDetail(@PathVariable String orderId) {

		
		ApiContext context = ApiContext.create(clientId,
				clientSecert,
				ApiContext.Mode.LIVE);
		Instamojo api = new InstamojoImpl(context);
		/*
		 * Get details of payment order when order id is given
		 */
		try {
		    PaymentOrder paymentOrder = api.getPaymentOrder(orderId);
		  
		    
		    System.out.println(paymentOrder.getId());
		    System.out.println(paymentOrder.getStatus());
		    return new Gson().toJson(paymentOrder);


		} catch (HTTPException e) {
		    System.out.println(e.getStatusCode());
		    System.out.println(e.getMessage());
		    System.out.println(e.getJsonPayload());

		} catch (ConnectionException e) {
		    System.out.println(e.getMessage());
		}

		return null;
	}
	
	
	
	
	
	static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    } 
	
	
	@RequestMapping(value = "/getOrderId", method = RequestMethod.POST ,  produces={"application/json"})
@ResponseBody
	public  GetOrderIDResponse getOrder(@org.springframework.web.bind.annotation.RequestBody GetOrderIDRequest getOrderIDRequest) {

		
		ApiContext context = ApiContext.create(clientId,
				clientSecert,
				ApiContext.Mode.TEST);
		Instamojo api = new InstamojoImpl(context);

		/*
		 * Create a new payment order
		 */
		PaymentOrder order = new PaymentOrder();
		order.setName(getOrderIDRequest.getBuyerName());
		order.setEmail(getOrderIDRequest.getBuyerEmail());
		order.setPhone(getOrderIDRequest.getBuyerPhone());
		order.setCurrency("INR");
		order.setAmount(Double.parseDouble(getOrderIDRequest.getAmount()));
		order.setDescription(getOrderIDRequest.getDescription());
		order.setRedirectUrl(RedirectUrl);
		order.setWebhookUrl(WebhookUrl);
		order.setTransactionId(getAlphaNumericString(15));

		try {
			PaymentOrderResponse paymentOrderResponse = api.createPaymentOrder(order);
			System.out.println(paymentOrderResponse.getPaymentOrder().getStatus());

			GetOrderIDResponse getOrderIDResponse=new GetOrderIDResponse();
			getOrderIDResponse.setOrderID(paymentOrderResponse.getPaymentOrder().getId());
		
			getOrderIDRequest.setOrderID(paymentOrderResponse.getPaymentOrder().getId());
			
			DbOperation.InsertInfo(getOrderIDRequest);
			
			
			return getOrderIDResponse;

		} catch (HTTPException e) {
			System.out.println(e.getStatusCode());
			System.out.println(e.getMessage());
			System.out.println(e.getJsonPayload());

		} catch (ConnectionException e) {
			System.out.println(e.getMessage());
		}

		return null;
	}
	
	
	
	
	

}
