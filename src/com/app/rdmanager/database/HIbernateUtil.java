package com.app.rdmanager.database;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


 
public class HIbernateUtil
{
   private static SessionFactory sessionFactory = buildSessionFactory();
   private static Session session = null ;

   private static SessionFactory buildSessionFactory()
   {
      try
      {
         if (sessionFactory == null)
         {
            Configuration configuration = new Configuration().configure(HIbernateUtil.class.getResource("/com/app/rdmanager/config/hibernate_ccavenue.cfg.xml"));
            StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
            serviceRegistryBuilder.applySettings(configuration.getProperties());
            ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
         }
         System.out.println("Database connected." );

         return sessionFactory;
      } catch (Throwable ex)
      {
         System.err.println("Initial SessionFactory creation failed." + ex);
         throw new ExceptionInInitializerError(ex);
      }
   }
 
   public static SessionFactory getSessionFactory()
   {
      return sessionFactory;
   }
   

   public static Session getSession()
   {
	
	   if(session==null)
	    session = getSessionFactory().openSession();
	   else if (!session.isOpen())
		    session = getSessionFactory().openSession();

	   
	   
	   
		   
	   return session;
	   
	   
	   
   }
   
   public static void shutdown()
   {
      getSessionFactory().close();
   }
   
   
   public static boolean isConnected()
   
   {
   
	   
	   Session session = getSessionFactory().openSession();
       return session!=null?true:false;	   
	   
   }
   
}
