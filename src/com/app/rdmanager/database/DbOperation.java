package com.app.rdmanager.database;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


//import mobile.info.web.model.Color;
//import mobile.info.web.model.Company;
//import mobile.info.web.model.Mobile;
//import mobile.info.web.model.mobileDisplay;
//import mobile.info.web.model.sensor;
//import mobile.info.web.model.usb;

public class DbOperation {
//
//	public static int InsertMobileInfo(Mobile mobile) {
//		Session session = null;
//
//		if (HIbernateUtil.isConnected()) {
//			try {
//				session = HIbernateUtil.getSessionFactory().openSession();
//				session.beginTransaction();
//
//					session.save(mobile);
//
//				session.getTransaction().commit();
//
//			} catch (Exception e) {
//				e.printStackTrace();
//				return mobile.getId();
//
//			}
//			session.close();
//
//		}
//		return  mobile.getId();
//	}

	public static <T> int InsertInfo(T object) {
		Session session = null;
		Transaction tx=null;
		int id = -1;
		try {
		if (HIbernateUtil.isConnected()) {
			
				session = HIbernateUtil.getSession();
				tx=session.beginTransaction();

				

				session.save(object);
//
//				if (object instanceof Color) {
//
//					System.out.println("Color");
//
//					id = ((Color) object).getId();
//
//				}else 
//					if (object instanceof Company) {
//
//					System.out.println("Company");
//
//				}
//				if (object instanceof usb) {
//
//					System.out.println("usb");
//
//					id = ((usb) object).getId();
//
//				}
//				if (object instanceof sensor) {
//
//					System.out.println("sensor");
//
//					id = ((sensor) object).getId();
//
//				}if (object instanceof mobileDisplay) {
//
//					System.out.println("sensor");
//
//					id = ((mobileDisplay) object).getId();
//
//				}

				tx.commit();
				session.close();
			

		}
		} catch (Exception e) {
			e.printStackTrace();

			tx.rollback();
			
			session.close();

			return id;

		}
		return id;
	}
	
	static public <T> void saveOrUpdate(final T o){
			Session session = null;

		 if (HIbernateUtil.isConnected()) {
				try {
					session = HIbernateUtil.getSession();
					session.beginTransaction();
					session.saveOrUpdate(o);
					session.getTransaction().commit();

				}catch(Exception e)
				{
					e.printStackTrace();
					session.close();

				}
				}
		 
	    }

	static public boolean exists(Class<?> clazz, String idKey, Object idValue) {
		return HIbernateUtil.getSession().createCriteria(clazz)
				.add(Restrictions.eq(idKey, idValue).ignoreCase()).setProjection(Projections.property(idKey))
				.uniqueResult() != null;
	}
	
	
	
	static public <T> T getObject(Class<?> clazz, String idKey, Object idValue) {
		return ((T)( HIbernateUtil.getSession().createCriteria(clazz)
				.add(Restrictions.eq(idKey, idValue).ignoreCase())
				.setMaxResults(1)
				.uniqueResult() ));
	}

}
