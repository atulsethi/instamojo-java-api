package com.app.ccavenue.database;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


//import mobile.info.web.model.Color;
//import mobile.info.web.model.Company;
//import mobile.info.web.model.Mobile;
//import mobile.info.web.model.mobileDisplay;
//import mobile.info.web.model.sensor;
//import mobile.info.web.model.usb;

public class CcAvenueDbOperation {
//
//	public static int InsertMobileInfo(Mobile mobile) {
//		Session session = null;
//
//		if (CcAvenuehibernateUtil.isConnected()) {
//			try {
//				session = CcAvenuehibernateUtil.getSessionFactory().openSession();
//				session.beginTransaction();
//
//					session.save(mobile);
//
//				session.getTransaction().commit();
//
//			} catch (Exception e) {
//				e.printStackTrace();
//				return mobile.getId();
//
//			}
//			session.close();
//
//		}
//		return  mobile.getId();
//	}

	public static <T> T InsertInfo(T object) {
		Session session = null;
		Transaction tx=null;
		int id = -1;
		try {
		if (CcAvenuehibernateUtil.isConnected()) {
			
				session = CcAvenuehibernateUtil.getSession();
				tx=session.beginTransaction();

				

				session.save(object);
				
				
				
				
//
//				if (object instanceof Color) {
//
//					System.out.println("Color");
//
//					id = ((Color) object).getId();
//
//				}else 
//					if (object instanceof Company) {
//
//					System.out.println("Company");
//
//				}
//				if (object instanceof usb) {
//
//					System.out.println("usb");
//
//					id = ((usb) object).getId();
//
//				}
//				if (object instanceof sensor) {
//
//					System.out.println("sensor");
//
//					id = ((sensor) object).getId();
//
//				}if (object instanceof mobileDisplay) {
//
//					System.out.println("sensor");
//
//					id = ((mobileDisplay) object).getId();
//
//				}

				tx.commit();
				session.close();
				return object;

		}
		} catch (Exception e) {
			e.printStackTrace();

			tx.rollback();
			
			session.close();

			return null;

		}
		return null;
	}
	
	static public <T> void saveOrUpdate(final T o){
			Session session = null;
			Transaction tx=null;
		 if (CcAvenuehibernateUtil.isConnected()) {
				try {
					session = CcAvenuehibernateUtil.getSession();
					tx=session.beginTransaction();
					session.saveOrUpdate(o);
					tx.commit();
					session.close();
				}catch(Exception e)
				{
					e.printStackTrace();
					session.close();

				}
				}
		 
	    }

	static public boolean exists(Class<?> clazz, String idKey, Object idValue) {
		return CcAvenuehibernateUtil.getSession().createCriteria(clazz)
				.add(Restrictions.eq(idKey, idValue).ignoreCase()).setProjection(Projections.property(idKey))
				.uniqueResult() != null;
	}
	
	
	
	static public <T> T getObject(Class<?> clazz, String idKey, Object idValue) {
		return ((T)( CcAvenuehibernateUtil.getSession().createCriteria(clazz)
				.add(Restrictions.eq(idKey, idValue).ignoreCase())
				.setMaxResults(1)
				.uniqueResult() ));
	}

}
