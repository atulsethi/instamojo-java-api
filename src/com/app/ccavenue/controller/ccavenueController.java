package com.app.ccavenue.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.app.ccavenue.database.CcAvenueDbOperation;
import com.app.ccavenue.database.CcAvenuehibernateUtil;
import com.app.ccavenue.model.Billing;
import com.app.ccavenue.model.CcAvaenueOrderRequest;
import com.app.ccavenue.model.CustomNotificationRecharge;
import com.app.ccavenue.model.Notification;
import com.app.ccavenue.model.Product;
import com.app.ccavenue.model.User;
import com.app.ccavenue.restclient.RestClient;
import com.app.ccavenue.utils.DateDeserializer;
import com.app.rdmanager.database.DbOperation;
import com.ccavenue.security.AesCryptUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Controller
@RequestMapping("/")
public class ccavenueController {

	String workingKey = "4A83CF2B88CCADC3D7459A98FB9FF4BE"; // Put in the 32 Bit Working Key provided by CCAVENUES.

	
	@RequestMapping(value = "ccavRequestHandler", method = RequestMethod.GET)
	public String ccavRequestHandlerPost(ModelMap model, @RequestParam(name = "packId") Integer packId,
			@RequestParam(name = "quantity") Integer quantity,@RequestParam(name = "userId") Integer userId) {

	
		System.out.println("packId: "+packId);

		System.out.println("quantity: "+quantity);

		System.out.println("userId: "+userId);

		
		
		Product mProduct = null;
		Session session = CcAvenuehibernateUtil.getSessionFactory().openSession();
		try {
			mProduct = (Product) session.get(Product.class, packId);
			System.out.println("mProduct: "+mProduct.toString());
		} catch (HibernateException e) {
			e.printStackTrace();
			/*if (transaction != null)
				transaction.rollback();*/
		} finally {
			session.close();
		}
		
		

		

		
		Integer amount = mProduct.getAmount() * quantity;
		
		String transactionId=Long.toString(System.currentTimeMillis()+(int) (Math.random()*9000)+1000);
		
		//String orderId=Long.toString(System.currentTimeMillis()+(int) (Math.random()*9000)+1000);
		
		Billing mBilling=new Billing();
		mBilling.setProduct_id(packId);
		mBilling.setAmount(amount);
		mBilling.setQuantity(quantity);
		mBilling.setT_id(transactionId);	
		mBilling.setUser_id(userId);
	//	mBilling.setOrder_id(orderId);
		mBilling=(Billing)	CcAvenueDbOperation.InsertInfo(mBilling);

		
		System.out.println("mBilling : "+mBilling.toString());
		
		HashMap<String, String> hasmap = new HashMap<String, String>();
		
		hasmap.put("tid", transactionId);
		hasmap.put("merchant_id", "253922");
		hasmap.put("order_id",Integer.toString(mBilling.getId()) );
		hasmap.put("amount", Integer.toString(amount));
		hasmap.put("currency", "INR");
		hasmap.put("redirect_url", "http://payment.gossipline.in/ccavResponseHandler");
		hasmap.put("cancel_url", "http://payment.gossipline.in/ccavResponseHandler");
		hasmap.put("language", "EN");
	
//		hasmap.put("billing_name", "EN");
//		hasmap.put("billing_address", "EN");
//		hasmap.put("billing_city", "EN");
//		hasmap.put("billing_state", "EN");
//		hasmap.put("billing_zip", "EN");
//		hasmap.put("billing_country", "EN");
//		hasmap.put("billing_tel", "EN");
//		hasmap.put("billing_email", "EN");
		
		String ccaRequest = "", pname = "", pvalue = "";

		for (Map.Entry<String, String> entry : hasmap.entrySet()) {
			pname = "" + entry.getKey();
			pvalue = entry.getValue();
			System.out.println("pname: " + pname + " pvalue:" + pvalue);

			try {
				ccaRequest = ccaRequest + pname + "=" + URLEncoder.encode(pvalue, "UTF-8") + "&";
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


		
		
		
	
		
		
		
		AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
		String encRequest = aesUtil.encrypt(ccaRequest);
		model.addAttribute("encRequest", encRequest);
		System.out.println("encRequest: " + encRequest );


		
		
		
		
		
		
		
//		RestClient mRestClient=	new RestClient();
//		System.out.println("Hit notificayion api");
//
//		CustomNotificationRecharge mCustomNotificationRecharge=new CustomNotificationRecharge();
//		mCustomNotificationRecharge.setmBilling(mBilling);
//		mCustomNotificationRecharge.setmProduct(mProduct);
//
//		try {
//			mRestClient.getRestService().recharge(mCustomNotificationRecharge).enqueue(new  Callback<Notification>() {
//				
//				@Override
//				public void onResponse(Call<Notification> call, Response<Notification> response) {
//					// TODO Auto-generated method stub
//					//System.out.println("Hit notificayion api" );
//				}
//				
//				@Override
//				public void onFailure(Call<Notification> call, Throwable t) {
//					t.getStackTrace();
//				}
//			});
//			
//			
//				} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		
		
		
	
		
		 return "ccavRequestHandler";
	}


	@RequestMapping(value = "ccavResponseHandler", method = RequestMethod.POST)
	public ModelAndView ccavResponseHandlerPost(@RequestParam Map<String,String> allRequestParams,ModelMap model) {
		
//Producation
	
//		pname: orderNo pvalue:1587715871487
//		pname: crossSellUrl pvalue:
//		encResp pname: order_id pvalue:1587715871487
//		encResp pname: tracking_id pvalue:109832999988
//		encResp pname: bank_ref_no pvalue:null
//		encResp pname: order_status pvalue:Failure   // encResp pname: order_status pvalue:Success
//		encResp pname: failure_message pvalue:
//		encResp pname: payment_mode pvalue:Debit Card
//		encResp pname: card_name pvalue:MasterCard Debit Card
//		encResp pname: status_code pvalue:null
//		encResp pname: status_message pvalue:NOT CAPTURED // encResp pname: status_message pvalue:SUCCESS
//		encResp pname: currency pvalue:INR
//		encResp pname: amount pvalue:1.00
//		encResp pname: billing_name pvalue:
//		encResp pname: billing_address pvalue:
//		encResp pname: billing_city pvalue:
//		encResp pname: billing_state pvalue:
//		encResp pname: billing_zip pvalue:
//		encResp pname: billing_country pvalue:
//		encResp pname: billing_tel pvalue:
//		encResp pname: billing_email pvalue:
//		encResp pname: delivery_name pvalue:
//		encResp pname: delivery_address pvalue:
//		encResp pname: delivery_city pvalue:
//		encResp pname: delivery_state pvalue:
//		encResp pname: delivery_zip pvalue:
//		encResp pname: delivery_country pvalue:
//		encResp pname: delivery_tel pvalue:
//		encResp pname: merchant_param1 pvalue:
//		encResp pname: merchant_param2 pvalue:
//		encResp pname: merchant_param3 pvalue:
//		encResp pname: merchant_param4 pvalue:
//		encResp pname: merchant_param5 pvalue:
//		encResp pname: vault pvalue:N
//		encResp pname: offer_type pvalue:null
//		encResp pname: offer_code pvalue:null
//		encResp pname: discount_value pvalue:0.0
//		encResp pname: mer_amount pvalue:1.00
//		encResp pname: eci_value pvalue:null
//		encResp pname: retry pvalue:N
//		encResp pname: response_code pvalue:0
//		encResp pname: billing_notes pvalue:
//		encResp pname: trans_date pvalue:24/04/2020 13:41:51
//		encResp pname: bin_country pvalue:INDIA


		
		
		
		for (Map.Entry<String, String> entry : allRequestParams.entrySet()) {
			String ccaRequest = "", pname = "", pvalue = "";
			pname = "" + entry.getKey();
			pvalue = entry.getValue();
			System.out.println("pname:" + pname + " pvalue:" + pvalue);

		}
		
		
		
		
		
		String encResp= allRequestParams.get("encResp");
		AesCryptUtil aesUtil=new AesCryptUtil(workingKey);
		String decResp = aesUtil.decrypt(encResp);
		StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
		Hashtable hs=new Hashtable();
		String pair=null, pname=null, pvalue=null;
		while (tokenizer.hasMoreTokens()) {
			pair = (String)tokenizer.nextToken();
			if(pair!=null) {
				StringTokenizer strTok=new StringTokenizer(pair, "=");
				pname=""; pvalue="";
				if(strTok.hasMoreTokens()) {
					pname=(String)strTok.nextToken();
					if(strTok.hasMoreTokens())
						pvalue=(String)strTok.nextToken();
					hs.put(pname, URLDecoder.decode(pvalue));
					System.out.println("encResp pname: " + pname + " pvalue:" + pvalue);
				}
			}
		}
		
	
		String OrderNo= allRequestParams.get("orderNo");
		
		Billing mBilling = null;
		Session session = CcAvenuehibernateUtil.getSessionFactory().openSession();
		
		System.out.println("OrderNo: "+OrderNo);

		try {
			mBilling = (Billing) session.get(Billing.class, Integer.parseInt(OrderNo));
			//System.out.println("mBilling: "+mBilling.toString());
		} catch (HibernateException e) {
			e.printStackTrace();
			
			/*if (transaction != null)
				transaction.rollback();*/
		} finally {
			session.close();
		
		}
		
		
		if(((String) hs.get("order_status")).equalsIgnoreCase("Success"))
		{
			
			// add min and sms is user account 
			
		
		//Billing mBilling=CcAvenueDbOperation.getObject(Billing.class, "order_id", OrderNo);
			
		
	
			
		
		Integer producatId= mBilling.getProduct_id();
		Integer quantity=mBilling.getQuantity();
		Integer userId=mBilling.getUser_id();
		
		
		Product mProduct = null;
		 session = CcAvenuehibernateUtil.getSessionFactory().openSession();
		//
		try {
			//transaction = session.beginTransaction();
			mProduct = (Product) session.get(Product.class, producatId);
			//transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			/*if (transaction != null)
			*///	transaction.rollback();
		} finally {
			session.close();
		}
		
		
		Integer sec = mProduct.getMin() * 60 * quantity;
		Integer sms =mProduct.getSms() * quantity;
		
		
		Transaction transaction = null;
		 session = CcAvenuehibernateUtil.getSessionFactory().openSession();
		 User	mUser=null;
		 try {
			transaction = session.beginTransaction();
		mUser = (User) session.get(User.class,userId);
		System.out.println("mUser: "+mUser.toString());
		mUser.setMin(mUser.getMin()+sec);
		mUser.setSms(mUser.getSms()+sms);
			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (transaction != null)
				transaction.rollback();
		} finally {
			session.close();
		}

		 
		 mBilling.setStatus(1);
		 
		// payment_mode
		 mBilling.setPayment_type((String) hs.get("payment_mode"));
		 
		 
		CcAvenueDbOperation.saveOrUpdate(mBilling);
		CcAvenueDbOperation.saveOrUpdate(mUser);
		System.out.println("mUser: "+mUser.toString());		
		
		RestClient mRestClient=	new RestClient();
		System.out.println("Hit notificayion api");

		CustomNotificationRecharge mCustomNotificationRecharge=new CustomNotificationRecharge();
		mCustomNotificationRecharge.setmBilling(mBilling);
		mCustomNotificationRecharge.setmProduct(mProduct);

		try {
			mRestClient.getRestService().recharge(mCustomNotificationRecharge ).enqueue(new  Callback<Notification>() {
				
				@Override
				public void onResponse(Call<Notification> call, Response<Notification> response) {
					// TODO Auto-generated method stub
					//System.out.println("Hit notificayion api" );
				}
				
				@Override
				public void onFailure(Call<Notification> call, Throwable t) {
					t.getStackTrace();
				}
			});
			
			
				} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		
		
		
			return new ModelAndView("redirect:/ccavPaymentSuccess");

			//return "ccavPaymentSuccess";
			
		}else
		{
			 mBilling.setStatus(0);
			CcAvenueDbOperation.saveOrUpdate(mBilling);

			return new ModelAndView("redirect:/ccavPaymentFailed");

			//return "ccavPaymentFailed";
		}
		
		
		
		//return "ccavResponseHandler";
	}


	@RequestMapping(value = "ccavPaymentFailed",method = RequestMethod.GET)
	public String failed(ModelMap model) {

		//model.addAttribute("message", "Hello Spring MVC Framework!");
		return "PaymentFailed";
	}

	@RequestMapping(value = "ccavPaymentSuccess",method = RequestMethod.GET)
	public String success(ModelMap model) {

		//model.addAttribute("message", "Hello Spring MVC Framework!");
		return "PaymentSuccess";
	}


}
