package com.app.ccavenue.restclient;

import java.util.Date;

import com.app.ccavenue.utils.DateDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
	RestApiInterface apiService;

	public RestClient() {

//		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//		interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
//		OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

		// final String BASE_URL = "http://3.85.70.32:3000/";

		final String BASE_URL = "http://localhost:8090/";

//		Gson gson = new GsonBuilder()
//
//				.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
//
//				.create();
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder
			    .create();
		
		

		Retrofit retrofit = new Retrofit.Builder()

				.baseUrl(BASE_URL)
				//.client(client)
				.addConverterFactory(GsonConverterFactory.create(gson))

				.build();

		apiService =

				retrofit.create(RestApiInterface.class);

	}

	public RestApiInterface getRestService() {

		return apiService;

	}

}
