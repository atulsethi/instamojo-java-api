package com.app.ccavenue.restclient;


import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.app.ccavenue.model.Billing;
import com.app.ccavenue.model.CustomNotificationRecharge;
import com.app.ccavenue.model.Notification;
import com.app.ccavenue.model.Product;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;


//https://guides.codepath.com/android/consuming-apis-with-retrofit

public interface RestApiInterface {
	
	@POST("/callme/notification/Recharge")
    public  Call<Notification> recharge(@Body CustomNotificationRecharge mCustomNotificationRecharge);
	
	  

	  
}
