package com.app.ccavenue.model;



import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;




@Entity
@Table(name = "users")
public class User implements Serializable {


	private static final long serialVersionUID = 2683317269832649079L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "USER_TABLE_GENERATOR")
	@TableGenerator(name = "USER_TABLE_GENERATOR", table = "PK_Sequences", pkColumnName = "Sequence_Name", pkColumnValue = "USER_SEQ", valueColumnName = "Sequence_Value", allocationSize = 1)
	@Column(name = "user_id", unique = true, nullable = false)
	private Integer userId;
	
	
	@Column(name = "redeem_min")
	private Integer redeem_min;
	
	@Column(name = "min")
	private Integer min;
	
	@Column(name = "sms")
	private Integer sms;

	@Column(name = "username", length = 512)
	private String username;
	
	@Column(name = "gender", length = 30)
	private String gender;
	
	@Column(name = "age", length = 30)
	private Integer age;
	
	@Column(name = "city", length = 512)
	private String city;
	
	@Column(name = "lang", length = 512)
	private String lang;
	
	@Column(name = "description", length = 512)
	private String description;
	
	@Column(name = "mobile", length = 30)
	private String mobile;
	
	@Column(name = "scmnotification", length = 512)
	private String scmnotification;
	
	@Column(name = "lastupdatetime", length = 512)
	private Date lastupdatetime;
	
	@Column(name = "lastlogintime", length = 512)
	private Date lastlogintime;
	
	@Column(name = "registerdatetime", length = 512)
	private Date registerdatetime;
	
	@Column(name = "customloginstatus", length = 512)
	private String customloginstatus;
	
	@Column(name = "asterikloginstatus", length = 512)
	private String asterikloginstatus;
	
	
	
	@Column(name = "accountstatus")
	private Integer accountstatus;

	
	
	
	
	
	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getSms() {
		return sms;
	}

	public void setSms(Integer sms) {
		this.sms = sms;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getScmnotification() {
		return scmnotification;
	}

	public void setScmnotification(String scmnotification) {
		this.scmnotification = scmnotification;
	}

	public Date getLastupdatetime() {
		return lastupdatetime;
	}

	public void setLastupdatetime(Date lastupdatetime) {
		this.lastupdatetime = lastupdatetime;
	}

	public Date getLastlogintime() {
		return lastlogintime;
	}

	public void setLastlogintime(Date lastlogintime) {
		this.lastlogintime = lastlogintime;
	}

	public Date getRegisterdatetime() {
		return registerdatetime;
	}

	public void setRegisterdatetime(Date registerdatetime) {
		this.registerdatetime = registerdatetime;
	}

	public String getCustomloginstatus() {
		return customloginstatus;
	}

	public void setCustomloginstatus(String customloginstatus) {
		this.customloginstatus = customloginstatus;
	}

	public String getAsterikloginstatus() {
		return asterikloginstatus;
	}

	public void setAsterikloginstatus(String asterikloginstatus) {
		this.asterikloginstatus = asterikloginstatus;
	}

	public Integer getAccountstatus() {
		return accountstatus;
	}

	public void setAccountstatus(Integer accountstatus) {
		this.accountstatus = accountstatus;
	}

	public Integer getRedeem_min() {
		return redeem_min;
	}

	public void setRedeem_min(Integer redeem_min) {
		this.redeem_min = redeem_min;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", redeem_min=" + redeem_min + ", min=" + min + ", sms=" + sms + ", username="
				+ username + ", gender=" + gender + ", age=" + age + ", city=" + city + ", lang=" + lang
				+ ", description=" + description + ", mobile=" + mobile + ", scmnotification=" + scmnotification
				+ ", lastupdatetime=" + lastupdatetime + ", lastlogintime=" + lastlogintime + ", registerdatetime="
				+ registerdatetime + ", customloginstatus=" + customloginstatus + ", asterikloginstatus="
				+ asterikloginstatus + ", accountstatus=" + accountstatus + "]";
	}
	
	
	
	
	
	
}
